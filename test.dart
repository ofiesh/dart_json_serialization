import "dart:mirrors";

int main() {

  ClassMirror cm = reflectClass(Foo);
  print(cm.superclass.reflectedType == Buildable);
  print((cm.declarations[#builder] as MethodMirror).returnType);
}

abstract class Buildable {
  builder();
}

class Foo extends Buildable {
  FooBuilder builder() {
    return new FooBuilder();
  }
}

class FooBuilder{
  build() => new Foo();
}