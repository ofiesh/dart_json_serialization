part of json_object_test;

class PrimativeObject {
  PrimativeObject();
  factory PrimativeObject.args(var identifier) {
    return new PrimativeObject()
      ..identifier = identifier
      ..n = 1.03
      ..string = "string"
      ..boolean = false
      ..i = 3
      ..d = 3.1415
      ..nulll = null;
  }
  var identifier;
  num n;
  String string;
  bool boolean;
  int i;
  double d;
  var nulll;
  isEqualsTo(PrimativeObject o) =>
    o.identifier == identifier
    && o.n == n
    && o.string == string
    && o.boolean == boolean
    && o.i == i
    && o.d == d
    && nulll == nulll;
}

class JustASetter {
  List<String> b;
  set a(List<String> a) => b = a;
}

class BuildableTest extends Buildable {
  final bool testable;
  BuilderTest builder() => new BuilderTest();
  BuildableTest(this.testable);
}

class BuilderTest extends Builder {
  bool _testable;
  BuildableTest build() => new BuildableTest(_testable);
  set testable(bool testable) => _testable = testable;
}

class ComplexObject {
  ComplexObject();
  factory ComplexObject.factory() =>
    new ComplexObject()
      ..privatePrimativeObject = new PrimativeObject.args("method")
      ..primativeObject = new PrimativeObject.args("variable")
      ..l = [new PrimativeObject.args("variableL1"), new PrimativeObject.args("variableL2")]
      ..m = {
        "m1": new PrimativeObject.args("variableM1"),
        "m2": new PrimativeObject.args("variableM2")
      };

  PrimativeObject _privatePrimativeObject;
  PrimativeObject get privatePrimativeObject => _privatePrimativeObject;
  set privatePrimativeObject(PrimativeObject primativeObject) => _privatePrimativeObject = primativeObject;
  PrimativeObject primativeObject;
  List<PrimativeObject> l;
  Map<Object, PrimativeObject> m;
  bool isEqualsTo(ComplexObject o) =>
    o.primativeObject.isEqualsTo(primativeObject)
    && o.privatePrimativeObject.isEqualsTo(privatePrimativeObject)
    && o.l[0].isEqualsTo(l[0])
    && o.l[1].isEqualsTo(l[1])
    && o.m["m1"].isEqualsTo(m["m1"])
    && o.m["m2"].isEqualsTo(m["m2"]);
  }

testMirrorsDeserialize() {
  JsonSerializer jsonSerializer = new JsonSerializer();
  group('deserializeMirrors', () {
    test('PrimativeObject', () {
      JsonDeserializer d = new JsonDeserializer(PrimativeObject);
      String json = '{"identifier":"abc","n":1.03,"string":"string","boolean":false,"i":3,"d":3.1415,"nulll":null}';
      expect(new PrimativeObject.args("abc").isEqualsTo(d.deserialize(json)), true);
    });
    test('ComplexObject', () {
      JsonDeserializer d = new JsonDeserializer(ComplexObject);
      String json = '{"primativeObject":{"identifier":"variable","n":1.03,"string":"string","boolean":false,"i":3,"d":3.1415,"nulll":null},"l":[{"identifier":"variableL1","n":1.03,"string":"string","boolean":false,"i":3,"d":3.1415,"nulll":null},{"identifier":"variableL2","n":1.03,"string":"string","boolean":false,"i":3,"d":3.1415,"nulll":null}],"m":{"m1":{"identifier":"variableM1","n":1.03,"string":"string","boolean":false,"i":3,"d":3.1415,"nulll":null},"m2":{"identifier":"variableM2","n":1.03,"string":"string","boolean":false,"i":3,"d":3.1415,"nulll":null}},"privatePrimativeObject":{"identifier":"method","n":1.03,"string":"string","boolean":false,"i":3,"d":3.1415,"nulll":null}}';
      expect(new ComplexObject.factory().isEqualsTo(d.deserialize(json)), true);
    });
    test('ContainsDateTime', () {
      JsonDeserializer d = new JsonDeserializer(ContainsDateTime);
      String json = '{"dateTime": 1389036120}';
      ContainsDateTime object = d.deserialize(json);
      expect(object.dateTime.millisecondsSinceEpoch, 1389036120);
    });
    test('containsDateTimeNull', () {
      JsonDeserializer d = new JsonDeserializer(ContainsDateTime);
      String json = '{"dateTime": null}';
      ContainsDateTime object = d.deserialize(json);
      expect(object.dateTime, null);
    });
    test('justASetter', () {
      JsonDeserializer d = new JsonDeserializer(JustASetter);
      String json = '{"a": ["xyz"]}';
      JustASetter object = d.deserialize(json);
      expect(object.b[0], "xyz");
    });
    test('buildableTest', () {
      JsonDeserializer d = new JsonDeserializer(BuildableTest);
      String json = '{"testable": true}';
      BuildableTest object = d.deserialize(json);
      expect(object.testable, true);
    });
  });
}