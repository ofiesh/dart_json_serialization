library json_object_test;

import "package:unittest/unittest.dart";

import "../lib/json_serialization.dart";

import "dart:convert";

part "test_mirrors_serialize.dart";
part "test_mirrors_deserialize.dart";

var enableJsonObjectDebugMessages;

void _log(obj) {
  if (enableJsonObjectDebugMessages) print(obj);
}

void main() {
  enableJsonObjectDebugMessages = true;

  testMirrorsSerialize(); // tests converting a class to JSON
  testMirrorsDeserialize(); // tests converting JSON to a class
}


