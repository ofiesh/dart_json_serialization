library json_serialization;

import "dart:convert";
@MirrorsUsed()
import "dart:mirrors";

part "src/serialization/serializer.dart";
part "src/serialization/deserializer.dart";

bool isPrimative(Object object){
  if (object is num ||
      object is bool ||
      object is String ||
      object == null) {
    return true;
  } else {
    return false;
  }
}

abstract class Buildable {
  Builder builder();
}

abstract class Builder {
  build();
}