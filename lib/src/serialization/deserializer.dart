part of json_serialization;

Symbol _emptyConstructor = const Symbol("");
Symbol _builderSymbol = #builder;
List _emptyArgs = [];

class JsonDeserializer {
  final _JsonObjectDeserializer _deserializer;

  JsonDeserializer._internal(this._deserializer);
  factory JsonDeserializer(Type type) =>
      new JsonDeserializer._internal(new _JsonObjectDeserializer(type));

  Object deserialize(String string) {
    Object result;
    //Outermost object valid of json must be a map or array
    Object json = JSON.decode(string);

    //If it's an array, assume each of the array elements are the Type to deserialize
    if(json is List) {
      //TODO would like type arguments
      result = new List();
      for(Object shouldBeMap in json) {
        (result as List).add(_deserializer.deserializeObject(shouldBeMap));
      }

    //If it's a map, assume the element is the Type to deserialize
    } else if(json is Map) {
      result = _deserializer.deserializeObject(json);

    //Not a map or array, not valid json
    } else {
      //TODO Enhance... Enhance... Enhance... Enhance...
      throw new Exception("Object must be a list or a map");
    }
    return result;
  }
}

class _JsonObjectDeserializer {
  final ClassMirror _classMirror;

  _JsonObjectDeserializer._internal(this._classMirror);
  factory _JsonObjectDeserializer(Type type) {
    ClassMirror cm = reflectClass(type);
    if(cm.superclass.reflectedType == Buildable) {
      ClassMirror builderClassMirror = (cm.declarations[_builderSymbol] as MethodMirror).returnType;
      //TODO check to make sure cm is a builder
        return new _JsonObjectDeserializer._internal(
            (cm.declarations[_builderSymbol] as MethodMirror).returnType);
    }
    return new _JsonObjectDeserializer._internal(cm);
  }

  Object deserializeObject(Map map) {
    InstanceMirror instanceMirror = _classMirror.newInstance(_emptyConstructor, _emptyArgs);
    map.forEach((k, v) {
      if(v != null) {
        Symbol key = new Symbol(k);
        //TODO I think they're fixing this to be consistent somewhere
        Symbol mirrorKey = new Symbol("$k=");
        DeclarationMirror dm = _classMirror.declarations[mirrorKey];
        if(dm == null)
          dm = _classMirror.declarations[key];
        Object value = deserializeDeclarationMirror(dm, v);
        instanceMirror.setField(key, value);
      }
    });
    if(instanceMirror.reflectee is Builder)
      return instanceMirror.reflectee.build();
    return instanceMirror.reflectee;
  }

  Object deserializeDeclarationMirror(DeclarationMirror declarationMirror, Object value) {
    var classMirror = null;
    if(declarationMirror is VariableMirror) {
      classMirror = declarationMirror.type;
    } else if(declarationMirror is MethodMirror && declarationMirror.isSetter) {
      classMirror = declarationMirror.parameters[0].type;
    }
    if(!isPrimative(value) ||
        classMirror != null && classMirror is ClassMirror && classMirror.reflectedType == DateTime) {
      var t = classMirror.runtimeType;
      _PropertyDeserializer propertyDeserializer = new _PropertyDeserializer(classMirror);
      return propertyDeserializer.deserializeProperty(value);
    }
    return value;
  }
}

class _PropertyDeserializer {
  final ClassMirror classMirror;
  _PropertyDeserializer(this.classMirror);

  Object deserializeProperty(Object value) {
    Object result;
    if(value is List) {
      result = _deserializeList(value);
    } else if(value is Map && classMirror.typeArguments.length == 2) {
      result = _deserializeMap(value);
    } else if(classMirror.reflectedType == DateTime) {
      result = new DateTime.fromMillisecondsSinceEpoch(value);
    } else {
      result = _deserializeObject(value);
    }
    return result;
  }

  Object _deserializeObject(Object json) =>
      new _JsonObjectDeserializer(classMirror.reflectedType).deserializeObject(json);

  List _deserializeList(List json) {
    List list = [];
    if(json.length > 0 && json[0] is Map) {
      _JsonObjectDeserializer deserializer
          = new _JsonObjectDeserializer((classMirror.typeArguments.first as ClassMirror).reflectedType);
      for(Map map in json) {
        list.add(deserializer.deserializeObject(map));
      }
    } else {
      for(var prim in json) {
        list.add(prim);
      }
    }
    return list;
  }

  Map _deserializeMap(Map json) {
    //TODO add type arguments
    Map map = {};
    _JsonObjectDeserializer deserializer
        = new _JsonObjectDeserializer((classMirror.typeArguments[1] as ClassMirror).reflectedType);
    json.forEach((k, v) => map[k] = deserializer.deserializeObject(v));
    return map;
  }
}