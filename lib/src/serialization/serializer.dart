part of json_serialization;

class JsonSerializer {
  Object _serialize(Object object) {
    Object result;
    if(isPrimative(object)) {
      result = object;
    } else if(object is DateTime) {
      result = object.millisecondsSinceEpoch;
    } else if(object is Map) {
      result = _serializeMap(object);
    } else if (object is List) {
      result = _serializeList(object);
    } else {
      result = _serializeObject(object);
    }
    return result;
  }

  String serialize(Object object) {
    Object objectToEncode = _serialize(object);
    String json = JSON.encode(objectToEncode);
    return json;
  }

  Object _serializeMap(Map map) {
    Map resultMap = {};

    map.forEach((k, v) {
      resultMap[k.toString()] = _serialize(v);
    });

    return resultMap;
  }

  Object _serializeList(List list) {
    List resultList = [];

    for(Object object in list) {
      resultList.add(_serialize(object));
    }

    return resultList;
  }

  Object _serializeObject(Object object) {
    Map resultMap = {};
    InstanceMirror instanceMirror = reflect(object);
    ClassMirror classMirror = instanceMirror.type;

    classMirror.declarations.forEach((Symbol k, DeclarationMirror v) {
      if((v is MethodMirror && v.isGetter || v is VariableMirror)
          && !v.isPrivate && !v.isStatic // throws _LocalMethodMirror is not a subtype of VariableMirror in type cast if casted to variable mirror
          ) {
       InstanceMirror getterInstanceMirror = instanceMirror.getField(k);
       if(isPrimative(getterInstanceMirror.reflectee)) {
         resultMap[MirrorSystem.getName(k)] = getterInstanceMirror.reflectee;
       } else {
         resultMap[MirrorSystem.getName(k)] = _serialize(getterInstanceMirror.reflectee);
       }
      }
    });

    return resultMap;
  }
}